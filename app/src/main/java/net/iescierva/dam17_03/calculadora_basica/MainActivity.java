package net.iescierva.dam17_03.calculadora_basica;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    public TextView pantalla;
    public double operando1, operando2, resultado;
    int ope;
    boolean punto = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pantalla = (TextView)findViewById(R.id.Pantalla);
    }

    public void boton1 (View v){
        String cap = pantalla.getText().toString();
        cap = cap + "1";
        pantalla.setText(cap);
    }

    public void boton2 (View v){
        String cap = pantalla.getText().toString();
        cap = cap + "2";
        pantalla.setText(cap);
    }

    public void boton3 (View v){
        String cap = pantalla.getText().toString();
        cap = cap + "3";
        pantalla.setText(cap);
    }

    public void boton4 (View v){
        String cap = pantalla.getText().toString();
        cap = cap + "4";
        pantalla.setText(cap);
    }

    public void boton5 (View v){
        String cap = pantalla.getText().toString();
        cap = cap + "5";
        pantalla.setText(cap);
    }

    public void boton6 (View v){
        String cap = pantalla.getText().toString();
        cap = cap + "6";
        pantalla.setText(cap);
    }

    public void boton7 (View v){
        String cap = pantalla.getText().toString();
        cap = cap + "7";
        pantalla.setText(cap);
    }

    public void boton8 (View v){
        String cap = pantalla.getText().toString();
        cap = cap + "8";
        pantalla.setText(cap);
    }

    public void boton9 (View v){
        String cap = pantalla.getText().toString();
        cap = cap + "9";
        pantalla.setText(cap);
    }

    public void boton0 (View v){
        String cap = pantalla.getText().toString();
        cap = cap + "0";
        pantalla.setText(cap);
    }

    public void botonPunto (View v){
        if(punto == false) {
            String cap = pantalla.getText().toString();
            cap = cap + ".";
            pantalla.setText(cap);
            punto = true;
        }
    }

    public void suma (View v){
        try{
            String aux1 = pantalla.getText().toString();
            operando1 = Double.parseDouble(aux1);
        }catch (NumberFormatException nfe){}
        pantalla.setText("");
        ope = 1;
    }

    public void resta (View v){
        try{
            String aux1 = pantalla.getText().toString();
            operando1 = Double.parseDouble(aux1);
        }catch (NumberFormatException nfe){}
        pantalla.setText("");
        ope = 2;
    }

    public void multiplicacion (View v){
        try{
            String aux1 = pantalla.getText().toString();
            operando1 = Double.parseDouble(aux1);
        }catch (NumberFormatException nfe){}
        pantalla.setText("");
        ope = 3;
    }

    public void division (View v){
        try{
            String aux1 = pantalla.getText().toString();
            operando1 = Double.parseDouble(aux1);
        }catch (NumberFormatException nfe){}
        pantalla.setText("");
        ope = 4;
    }

    public void igual (View v){
        try{
            String aux2 = pantalla.getText().toString();
            operando2 = Double.parseDouble(aux2);
        }catch (NumberFormatException nfe){}
        pantalla.setText("");

        if (ope == 1){
            resultado = operando1 + operando2;
        }else if (ope == 2){
            resultado = operando1 - operando2;
        }else if (ope == 3){
            resultado = operando1 * operando2;
        }else if (ope == 4){
            if(operando2 == Double.NaN)
                pantalla.setText("Error");
            else{
                resultado = operando1 / operando2;
            }
        }
        pantalla.setText(""+resultado);
        operando1 = resultado;
    }

    public void CE (View v){
        pantalla.setText("");
        operando1 = 0.0;
        operando2 = 0.0;
        resultado = 0.0;
    }

    public void C (View v){
        if(!pantalla.getText().toString().equals("")){
            pantalla.setText(pantalla.getText().subSequence(0,pantalla.getText().length()-1 )+"");
        }
    }


}
